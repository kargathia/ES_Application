#pragma once

#include <ControllerCommand.h>
#include <QueueHandler.h>
#include <libusb-1.0/libusb.h>
#include <string>
#include "ControllerConst.h"
#include "Device.h"

struct HandlerData {
  bool running;
  bool callbackFailed;
  Device device;
  QueueHandler<ControllerCommand> queue;
  SharedHandler<ControllerState> shared;
};

class DevicePublisher {
 public:
  DevicePublisher();
  virtual ~DevicePublisher();

  void StartAsync(libusb_device* dev, const std::string& sharedName, const std::string& queueName);
  void StopAsync();

  const std::string& GetSharedName() const {
    return sharedFile;
  }

  const std::string& GetQueueName() const {
    return queueFile;
  }

  libusb_device* GetUSBDevice() {
    return usbDevice;
  }

 private:
  sem_t switchSem;
  HandlerData handlers;

  std::string sharedFile;
  std::string queueFile;

  libusb_device* usbDevice;
  pthread_t infoThread;
  pthread_t commandThread;

 private:
  DevicePublisher(DevicePublisher&);  // NOLINT
};
