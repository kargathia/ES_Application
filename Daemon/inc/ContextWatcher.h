#pragma once

#include <Context.h>
#include <Device.h>
#include <DeviceAnnouncement.h>
#include <SharedHandler.h>
#include <libusb-1.0/libusb.h>
#include <string>
#include <vector>
#include "DevicePublisher.h"

class ContextWatcher {
 public:
  ContextWatcher(int vendorID, int productID, const std::string& announceSharedFile);
  virtual ~ContextWatcher();

  void StartAsync();
  void StopAsync();

  void AddPublisher(struct libusb_device* dev);
  void RemovePublisher(struct libusb_device* dev);
  void Announce();

  bool IsRunning() const {
    return running;
  }

  Context* GetContext() {
    return &context;
  }

 public:
  const int vendorID;
  const int productID;
  const std::string announceFileName;

 private:
  bool running;
  pthread_t thread;
  sem_t switchSem;

  Context context;
  std::vector<DevicePublisher*> publishers;
  SharedHandler<DeviceAnnouncement> announcer;
};
