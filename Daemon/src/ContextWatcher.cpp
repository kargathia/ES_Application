#include <Context.h>
#include <SSTR.h>
#include <SemHandler.h>
#include <glog/logging.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <strcpy_safe.h>
#include <iostream>
#include <stdexcept>
#include "ContextWatcher.h"

int hotplug_callback(struct libusb_context *ctx, struct libusb_device *dev, libusb_hotplug_event event,
                     void *user_data);
void *Run(void *args);

ContextWatcher::ContextWatcher(int vendor, int product, const std::string &announceSharedFile)
    : vendorID(vendor), productID(product), announceFileName(announceSharedFile), running(false) {
  sem_init(&switchSem, 0, 1);
}

ContextWatcher::~ContextWatcher() {
  StopAsync();
  sem_close(&switchSem);
}

void ContextWatcher::AddPublisher(struct libusb_device *dev) {
  sem_wait(&switchSem);
  // Because publishers are non-copyable (they control lifecycles of IO objects)
  // they need to be heap allocated in order to be contained in a collection
  // The better alternative would be to use smart pointers, as they innately track ref count
  DevicePublisher *publisher = new DevicePublisher();
  int slot = publishers.size();
  std::string sharedFileName = SSTR("sharedfile_" << slot << "_" << dev);
  std::string queueFileName = SSTR("/tmp/queuefile_" << slot << "_" << dev);
  publisher->StartAsync(dev, sharedFileName, queueFileName);

  publishers.push_back(publisher);
  Announce();
  sem_post(&switchSem);
}

void ContextWatcher::RemovePublisher(struct libusb_device *dev) {
  sem_wait(&switchSem);
  for (size_t i(0); i < publishers.size(); ++i) {
    if (dev == publishers.at(i)->GetUSBDevice()) {
      DevicePublisher *publisher = publishers.at(i);
      publishers.erase(publishers.begin() + i);
      delete publisher;
      Announce();
      break;
    }
  }
  sem_post(&switchSem);
}

void ContextWatcher::Announce() {
  LOG(INFO) << "Announcing " << publishers.size() << " devices";
  DeviceAnnouncement announcement;
  announcement.numConnected = publishers.size();

  for (size_t i(0); i < publishers.size(); ++i) {
    DevicePublisher *publisher = publishers.at(i);
    strcpy_safe(announcement.devices[i].infoFile, publisher->GetSharedName().c_str());
    strcpy_safe(announcement.devices[i].commandFile, publisher->GetQueueName().c_str());
  }
  announcer.Write(&announcement);
}

void ContextWatcher::StartAsync() {
  StopAsync();
  sem_wait(&switchSem);
  announcer.Init(announceFileName);
  LOG(INFO) << "starting watcher thread";
  running = true;
  pthread_create(&thread, NULL, &Run, this);
  sem_post(&switchSem);
}

void ContextWatcher::StopAsync() {
  sem_wait(&switchSem);
  if (running) {
    running = false;
    pthread_join(thread, NULL);

    for (size_t i(0); i < publishers.size(); ++i) {
      DevicePublisher *publisher = publishers.at(i);
      publisher->StopAsync();
      delete publisher;
    }
    publishers.clear();
  }
  sem_post(&switchSem);
}

void *Run(void *args) {
  LOG(INFO) << "Running contextwatcher";
  ContextWatcher *watcher = (ContextWatcher *)args;
  Context *context = watcher->GetContext();
  libusb_hotplug_callback_handle arrivedHandle;
  libusb_hotplug_callback_handle leftHandle;

  if (LIBUSB_SUCCESS != libusb_hotplug_register_callback(context->Get(), LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED,
                                                         LIBUSB_HOTPLUG_ENUMERATE, watcher->vendorID,
                                                         watcher->productID, LIBUSB_HOTPLUG_MATCH_ANY, hotplug_callback,
                                                         watcher, &arrivedHandle)) {
    LOG(INFO) << "Unable to register arrived callback";
    return NULL;
  }

  if (LIBUSB_SUCCESS != libusb_hotplug_register_callback(context->Get(), LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT,
                                                         LIBUSB_HOTPLUG_ENUMERATE, watcher->vendorID,
                                                         watcher->productID, LIBUSB_HOTPLUG_MATCH_ANY, hotplug_callback,
                                                         watcher, &leftHandle)) {
    LOG(INFO) << "Unable to register left callback";
    return NULL;
  }

  while (watcher->IsRunning()) {
    libusb_handle_events_completed(context->Get(), NULL);
    usleep(10000);
  }

  LOG(INFO) << "Shutting down context watcher runner";
  libusb_hotplug_deregister_callback(context->Get(), arrivedHandle);
  libusb_hotplug_deregister_callback(context->Get(), leftHandle);

  return 0;
}

int hotplug_callback(struct libusb_context *ctx, struct libusb_device *dev, libusb_hotplug_event event,
                     void *user_data) {
  ContextWatcher *watcher = (ContextWatcher *)user_data;

  if (LIBUSB_HOTPLUG_EVENT_DEVICE_ARRIVED == event) {
    LOG(INFO) << "Adding device";
    watcher->AddPublisher(dev);
  } else if (LIBUSB_HOTPLUG_EVENT_DEVICE_LEFT == event) {
    LOG(INFO) << "Removing device";
    watcher->RemovePublisher(dev);
  }

  return 0;
}
