#include <Context.h>
#include <ControllerConst.h>
#include <ControllerState.h>
#include <Device.h>
#include <SSTR.h>
#include <SemHandler.h>
#include <SharedHandler.h>
#include <glog/logging.h>
#include <libusb-1.0/libusb.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include "ContextWatcher.h"

sem_t exitSem;

void SigCatcher(int signum, siginfo_t* info, void* ptr) {
  LOG(INFO) << "caught " << signum << ", shutting down...";
  sem_post(&exitSem);
}

int main(int argc, char** argv) {
  FLAGS_logtostderr = 1;
  std::string logFile = SSTR("/var/log/ControllerDaemon.log");
  system(SSTR("rm " << logFile << "*").c_str());
  google::InitGoogleLogging(argv[0]);
  google::SetLogDestination(google::INFO, logFile.c_str());
  daemon(1, 1);

  LOG(INFO) << "Starting daemon";
  sem_init(&exitSem, 0, 0);

  // ensures SIGTERM and SIGINT are caught and handled properly
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_sigaction = SigCatcher;
  action.sa_flags = SA_SIGINFO;
  sigaction(SIGTERM, &action, NULL);
  sigaction(SIGINT, &action, NULL);

  ContextWatcher watcher(Const::VENDOR_ID, Const::PRODUCT_ID, Const::ANNOUNCE_FILE);
  watcher.StartAsync();

  sem_wait(&exitSem);
  watcher.StopAsync();

  LOG(INFO) << "Exiting main";

  return 0;
}
