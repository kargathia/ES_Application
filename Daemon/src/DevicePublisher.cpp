#include <SharedHandler.h>
#include <glog/logging.h>
#include <pthread.h>
#include <sched.h>
#include <stdexcept>
#include "DevicePublisher.h"

void* PublishInfo(void* args);
void* ListenCommands(void* args);
void OnDataRead(bool success, ControllerState* state, void* userData);

DevicePublisher::DevicePublisher() {
  handlers.running = false;
  handlers.callbackFailed = false;
  sem_init(&switchSem, 0, 1);
}

DevicePublisher::~DevicePublisher() {
  StopAsync();
  sem_close(&switchSem);
}

void DevicePublisher::StartAsync(libusb_device* dev, const std::string& sharedName, const std::string& queueName) {
  sem_wait(&switchSem);
  LOG(INFO) << "starting publisher async (" << sharedName << ") (" << queueName << ")";
  sharedFile = sharedName;
  queueFile = queueName;
  usbDevice = dev;

  handlers.shared.Init(sharedFile);
  handlers.queue.Init(queueFile, true);
  handlers.device.Init(dev);

  handlers.running = true;
  handlers.callbackFailed = false;

  pthread_attr_t attr;
  sched_param param;
  pthread_attr_init(&attr);
  pthread_attr_setschedpolicy(&attr, SCHED_FIFO);

  // to avoid thread starvation we'll set the info thread priority to low
  param.sched_priority = sched_get_priority_min(SCHED_FIFO);
  pthread_attr_setschedparam(&attr, &param);
  pthread_create(&infoThread, NULL, &PublishInfo, &handlers);

  // ... and the command thread priority to high
  param.sched_priority = sched_get_priority_max(SCHED_FIFO);
  pthread_attr_setschedparam(&attr, &param);
  pthread_create(&commandThread, &attr, &ListenCommands, &handlers);
  sem_post(&switchSem);
}

void DevicePublisher::StopAsync() {
  sem_wait(&switchSem);
  if (handlers.running) {
    LOG(INFO) << "shutting down publisher: " << sharedFile;
    handlers.running = false;

    handlers.device.Shutdown();
    handlers.shared.Shutdown();
    handlers.queue.Shutdown(true);

    pthread_join(infoThread, NULL);
    pthread_join(commandThread, NULL);
  }
  sem_post(&switchSem);
}

void* PublishInfo(void* args) {
  HandlerData* handlers = (HandlerData*)args;

  handlers->device.Send(Const::AllOff);
  handlers->device.SendRumble(0, 0);

  ControllerState state;
  while (handlers->running) {
    try {
      if (handlers->device.Read(&state)) {
        handlers->shared.Write(&state);
      }
      usleep(1000);
    } catch (std::runtime_error& ex) {
      LOG(WARNING) << "publisher error: " << ex.what();
    }
  }
  return NULL;
}

void* ListenCommands(void* args) {
  HandlerData* handlers = (HandlerData*)args;

  ControllerCommand command;

  while (handlers->running) {
    try {
      handlers->queue.Read(&command);

      if (command.action != Const::UNKNOWN) {
        LOG(INFO) << "sending command: " << Const::ActionToString(command.action);
        handlers->device.Send(command.action);
      }

      if (command.rumbleCommand) {
        LOG(INFO) << "sending rumble command: " << int(command.heavyRumble) << ", " << int(command.lightRumble);
        handlers->device.SendRumble(command.heavyRumble, command.lightRumble);
      }
    } catch (std::exception& ex) {
      LOG(WARNING) << "commandListener error: " << ex.what();
    }
  }
  return NULL;
}
