NORMAL      := 
CROSS       := arm-linux-

MODULES := Utils \
           SharedMemory \
           Device
CGI     := CGI-ControllerInfo \
           CGI-ControllerCommands \
					 CGI-ControllerRumble
TEST    := $(MODULES) Test
DAEMON  := $(MODULES) Daemon
ALL     := $(MODULES) $(CGI) Test Daemon
RELEASE := $(MODULES) $(CGI) Daemon

.PHONY: install install-arm deploy $(ALL) all

all: CXXTYPE=$(NORMAL)
all: $(ALL)

arm: CXXTYPE=$(CROSS)
arm: $(RELEASE)

test: CXXTYPE=$(NORMAL)
test: $(TEST)
	@cd Test/bin && ./Test

daemon: CXXTYPE=$(NORMAL)
daemon: $(DAEMON)

clean: TARGET=clean
clean: $(ALL)

cleaner: TARGET=cleaner
cleaner: $(ALL)

$(ALL):
	@echo "Making ***$@***"
	@cd $@ && make --no-print-directory $(TARGET) CXXTYPE=${CXXTYPE}

deploy: arm
	$(shell ./deploy.sh)
