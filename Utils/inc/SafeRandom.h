// Downloaded from http://stackoverflow.com/questions/21237905/how-do-i-generate-thread-safe-uniform-random-numbers
// 2016-12-25
// Adjusted to take default arguments for <max> and <min>

#pragma once

#include <limits.h>
#include <random>

namespace SafeRandom {
inline int Int(const int& min = INT_MIN, const int& max = INT_MAX) {
  static thread_local std::mt19937 generator;
  std::uniform_int_distribution<int> distribution(min, max);
  return distribution(generator);
}
}
