// Made obsolete by C++11 std::to_string() function
// Still pretty useful whenever C++11 is not available

#ifndef SSTR
#include <sstream>
#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()
#endif 

// Examples:
// int i = 42;
// std::cout << SSTR( "i is: " << i );
// std::string s = SSTR( i );
// puts( SSTR( i ).c_str() );
