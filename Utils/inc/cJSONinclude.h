#pragma once

// BuildRoot is still using an old version of cJSON
// If <string> is not included before cJSON.h, it can't find malloc_fn

#include <string>

#include <cJSON.h>  // NOLINT
