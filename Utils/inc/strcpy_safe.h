// Downloaded from https://randomascii.wordpress.com/2013/04/03/stop-using-strncpy-already/ 2016-12-25

#pragma once

template <size_t charCount>
void strcpy_safe(char (&output)[charCount], const char* pSrc) {
  // YourCopyNFunction(output, pSrc, charCount);
  // Copy the string — don’t copy too many bytes.
  strncpy(output, pSrc, charCount);
  // Ensure null-termination.
  output[charCount - 1] = 0;
}

// // Call it like this:
// char buffer[5];
// strcpy_safe(buffer, “Thisisalongstring”);
