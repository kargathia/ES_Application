#include <ControllerCommand.h>
#include <ControllerConst.h>
#include <DeviceAnnouncement.h>
#include <QueueHandler.h>
#include <SSTR.h>
#include <SharedHandler.h>
#include <cJSONinclude.h>
#include <cgicc/CgiDefs.h>
#include <cgicc/CgiEnvironment.h>
#include <cgicc/Cgicc.h>
#include <cgicc/HTMLClasses.h>
#include <cgicc/HTTPResponseHeader.h>
#include <stdlib.h> /* atoi */
#include <map>
#include <stdexcept>

int main(int argc, char **argv) {
  try {
    cgicc::Cgicc cgi;
    int responseCode = 200;
    std::string responseContent;

    std::map<std::string, std::string> args;
    cgicc::const_form_iterator iter;
    for (iter = cgi.getElements().begin(); iter != cgi.getElements().end(); ++iter) {
      args[iter->getName()] = iter->getValue();
    }

    std::string strSlot = args["slot"];
    std::string strCommand = args["command"];

    try {
      SharedHandler<DeviceAnnouncement> announceReader;
      DeviceAnnouncement announcement;
      announceReader.Init(Const::ANNOUNCE_FILE);
      announceReader.Read(&announcement);

      int numControllers = announcement.numConnected;
      int targetSlot = atoi(strSlot.c_str());
      if (numControllers <= targetSlot) {
        throw std::invalid_argument("invalid slot");
      }

      QueueHandler<ControllerCommand> queue;
      std::string queueFile = announcement.devices[targetSlot].commandFile;
      queue.Init(queueFile, false);

      ControllerCommand command;
      command.action = Const::StringToAction(strCommand);

      queue.Write(&command);
      // format request content as JSON, and return it
      cJSON *root;
      root = cJSON_CreateObject();
      cJSON_AddStringToObject(root, "command", args["command"].c_str());
      cJSON_AddStringToObject(root, "slot", args["slot"].c_str());
      responseContent = SSTR(cJSON_Print(root));
      cJSON_Delete(root);
    } catch (std::exception &ex) {
      responseCode = 503;  // internal server error
      responseContent = SSTR("Error: " << ex.what());
    }

    // Send HTTP header
    // Disable caching this request
    cgicc::HTTPResponseHeader response("HTTP/1.1", responseCode, "");
    response.addHeader("Content-Type", "application/json; charset=utf-8");
    response.addHeader("Pragma-directive", "no-cache");
    response.addHeader("Cache-directive", "no-cache");
    response.addHeader("Cache-control", "no-store");
    response.addHeader("Pragma", "no-cache");
    response.addHeader("expires", "0");
    std::cout << response << std::endl;
    std::cout << responseContent << std::endl;
  } catch (std::exception &e) {
    std::cout << "Error: " << e.what();
  }

  return 0;
}
