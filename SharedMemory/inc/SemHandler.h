#pragma once

#include <fcntl.h> /* For O_* constants */
#include <semaphore.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

class SemHandler {
 public:
  explicit SemHandler(std::string name, int count = 1);
  virtual ~SemHandler();
  sem_t* Get() {
    return mySem;
  }

 private:
  sem_t* OpenSem(const char* address, int count);
  sem_t* mySem;
  std::string mySemName;
};
