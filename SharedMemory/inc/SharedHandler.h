#pragma once

#include <SSTR.h>
#include <fcntl.h> /* For O_* constants */
#include <glog/logging.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include "SemHandler.h"

/*
A generic handler for reading and writing shared memory.
Its container class is client-defined by T.

WARNING: using SharedHandler with a T of dynamic memory size will cause segfaults, as it is unclear how much shared
memory should be reserved, written, or read.

For this reason, SharedHandler should not be used with std::string, or any variable-size collection type
*/
template <class T>
class SharedHandler {
 public:
  /*
  Constructor.
  Example:

  SharedHandler<MyStructOrClass> handler;
  */
  inline SharedHandler();

  /*
  Destructor. Will release all resources.
  Underlying shared file will only be fully closed if all SharedHandlers using it are shut down.
  */
  inline ~SharedHandler();

  /*
  Pre: -
  Post: Shared memory at file <file> is now initialized and truncated to sizeof(T)

  throws std::runtime_exception and std::invalid_argument
  If an error is thrown, all underlying resources are released.
  */
  inline void Init(std::string file);

  /*
  Pre: SharedHandler was successfully initialized
  Post: <val> was copied to the shared memory

  Threadsafe.
  throws std::invalid_argument if SharedHandler was not initialized, or <val> was a null pointer
  */
  inline void Write(T* val);

  /*
  Pre: SharedHandler was successfully initialized
  Post: shared memory was read, and copied to <output>

  Threadsafe.
  throws std::invalid_argument if SharedHandler was not initialized
  */
  inline void Read(T* output);

  /*
  Pre: -
  Post: All shared resources are released

  Releases all shared resources. Does not throw exceptions.
  Local resources remain allocated.
  Can safely be re-initialized afterwards.
  */
  inline void Shutdown();

  /*
  Pre: -
  Post: All shared resources are released. Currently held shared memory file is unlinked

  Any newly created SharedHandlers will not be able to read previously written data.
  SharedHandlers existing prior to destruction will not be notified.
  Read() calls made by them will continue to return the last object written prior to destruction
  */
  inline void Destroy();

 private:
  bool valid;
  int sharedFD;
  int bufferSize;
  std::string fileName;

 private:  // non-RAII objects
  void* vaddr;
  SemHandler* fileSemaphore;
};

//====================================================================
// Function definitions start here
// All are declared inline to allow for late compilation
// If functions were compiled now, and a client would use it with T == <SomeCustomClientType>,
// compilation would fail, as SharedHandler<SomeCustomClientType> was never compiled before linking
//====================================================================

template <class T>
inline SharedHandler<T>::SharedHandler() : valid(false), fileSemaphore(NULL) {
  bufferSize = sizeof(T);
}

template <class T>
inline SharedHandler<T>::~SharedHandler() {
  Shutdown();
}

template <class T>
inline void SharedHandler<T>::Destroy() {
  if (!fileName.empty()) {
    shm_unlink(fileName.c_str());
  }
  Shutdown();
}

template <class T>
inline void SharedHandler<T>::Shutdown() {
  valid = false;
  if (fileSemaphore != NULL) {
    delete fileSemaphore;
    fileSemaphore = NULL;
  }
  if (vaddr != NULL) {
    munmap(vaddr, bufferSize);
    vaddr = NULL;
  }
  if (sharedFD > 0) {
    close(sharedFD);
    sharedFD = -1;
  }
  fileName = "";
}

template <class T>
inline void SharedHandler<T>::Init(std::string file) {
  Shutdown();  // unlink previous shared file

  fileName = file;

  /* get shared memory handle */
  if ((sharedFD = shm_open(fileName.c_str(), O_CREAT | O_RDWR, 0666)) == -1) {
    std::string error = SSTR("cannot open: " << strerror(errno) << "(" << file << ")");
    Shutdown();
    throw std::runtime_error(error);
  }

  /* set the shared memory size to the size of T */
  if (ftruncate(sharedFD, bufferSize) != 0) {
    std::string error = SSTR("cannot set size: " << strerror(errno));
    Shutdown();
    throw std::runtime_error(error);
  }

  /* Map shared memory in address space. MAP_SHARED flag tells that this is a
  * shared mapping */
  if ((vaddr = mmap(0, bufferSize, PROT_WRITE | PROT_READ, MAP_SHARED, sharedFD, 0)) == MAP_FAILED) {
    std::string error = SSTR("cannot mmap: " << strerror(errno));
    Shutdown();
    throw std::runtime_error(error);
  }

  /* lock the shared memory */
  if (mlock(vaddr, bufferSize) != 0) {
    std::string error = SSTR("cannot mlock: " << strerror(errno));
    Shutdown();
    throw std::runtime_error(error);
  }

  // get a valid semaphore name from filename
  std::string semName = fileName;
  std::replace(semName.begin(), semName.end(), '/', '_');
  semName = "/" + semName;

  // set up sem handler
  try {
    fileSemaphore = new SemHandler(semName);
    if (fileSemaphore->Get() == NULL) {
      throw std::runtime_error("Failed initiating file semaphore");
    }
  } catch (std::exception& ex) {
    Shutdown();
    throw ex;
  }

  valid = true;
}

template <class T>
inline void SharedHandler<T>::Write(T* val) {
  if (!valid) {
    throw std::invalid_argument("SharedHandler was not initialized prior to using");
  }
  if (val == NULL) {
    throw std::invalid_argument("input was null");
  }

  // If we're going to segfault on val, better do it now
  uint8_t testVal = ((uint8_t*)val)[bufferSize];
  testVal++;  // avoid -Wall errors

  sem_wait(fileSemaphore->Get());
  memcpy(vaddr, val, bufferSize);
  sem_post(fileSemaphore->Get());
}

template <class T>
inline void SharedHandler<T>::Read(T* output) {
  if (!valid) {
    throw std::invalid_argument("SharedHandler was not initialized prior to using");
  }

  // If we're going to segfault on output, better do it now
  uint8_t testVal = ((uint8_t*)output)[bufferSize];
  testVal++;  // avoid -Wall errors

  sem_wait(fileSemaphore->Get());
  memcpy(output, vaddr, bufferSize);
  sem_post(fileSemaphore->Get());
}
