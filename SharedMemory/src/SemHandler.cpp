#include <glog/logging.h>
#include "SemHandler.h"

SemHandler::SemHandler(std::string name, int count) : mySemName(name) {
  mySem = OpenSem(name.c_str(), count);
}

SemHandler::~SemHandler() {
  sem_unlink(mySemName.c_str());
}

sem_t* SemHandler::OpenSem(const char* address, int count) {
  sem_t* ret = sem_open(address, O_CREAT, S_IWUSR | S_IRUSR | S_IROTH | S_IWOTH, count);
  if (ret == NULL) {
    LOG(ERROR) << "failed to open semaphore: " << strerror(errno);
    throw std::runtime_error("failed to open semaphore");
  }
  return ret;
}
