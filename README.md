## Shared Memory setup 

**Connection info** Shared memory describing what controllers are currently connected
* Daemon writes, CGI reads
* formatted as an array of info where to find shared memory / message queue eg. 
```
controllerinfo[0] = { 
  bool connected = true, 
  char[40] sharedFile = "ctrl1Shared", 
  char[40] commandQueue = "ctrl1Queue"
  }
```
* Every time a controller connects or disconnects, the shared memory is updated
* Every time a controller connects, its shared memory and message queue are started
* Every time a controller disconnects, its shared memory and message queue are destroyed

**Controller info** Shared memory describing controller state. 
* Daemon writes, CGI reads
* Daemon starts a new one for every connected controller

**Command Queue** Message queue where CGI tells controller what to do (rumble, lights)
* CGI writes, Daemon reads
* Daemon starts a new one for every connected controller

## Threading setup

### Daemon

* One thread for reading controller states. Loops through following actions:
  * Check what controllers are connected. Update shared memory with conection info
  * (for each controller) read button/trigger state, and write shared memories
* For each controller a thread reading its message queue for input commands

### CGI

* CGI scripts are called by whatever thread runs them. This is not directly under our control.
* CGI action inits message queue or shared memory, sends command or reads memory, and shuts it down.
* Web interfacing is handled with JavaScript. This reads button clicks to input commands, and continuously sends requests for updates.

## Buildroot packages
* cgicc
* httpd
* wchar
* glog
* cJSON

## Copy Buildroot settings
Buildroot configuration (used packages, etc) is saved in repository root as 'buildroot_config'.
To use this, copy it to '**buildroot repository root**/.config'.
After copying it: make buildroot, and copy its output to the raspberry ssd

## Httpd init
* create /etc/init.d/httpd script, with content `httpd -h /var/www`
* add `::sysinit:/etc/init.d/httpd` to /etc/inittab

## Deployment
* Add an entry in /etc/hosts that maps the raspberry IP address to 'raspberry'
* Run the following code to install your public ssh key on the raspberry:
```
ssh-keygen -R raspberry
ssh-keygen -R $(getent hosts raspberry | awk '{printf $1 }')
ssh-copy-id root@raspberry
```
* Use `make deploy` to compile all relevant modules as ARM, and scp them to the raspberry

## Command protocol
Commands are sent to the server using the following syntax:
```
slot = (slot id, 0 indexed)
command = (command, corresponds to command names in const.h)
value = (input value. relevant for rumblers. will always be 1 otherwise)
```
