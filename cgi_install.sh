#! /bin/sh

if ! [ $(id -u) = 0 ]; then
   echo "I am not root! Re-run this script using sudo."
   exit 1
fi

wget ftp://ftp.gnu.org/gnu/cgicc/cgicc-3.2.16.tar.gz
[ -d cgicc ] && rm -rf cgicc
mkdir -p cgicc
tar xf cgicc-3.2.16.tar.gz -C cgicc --strip-components=1
mv cgicc /usr/include
apt install build-essential libcgicc5-dev libcgicc-doc
apt update
rm -rf cgicc*
