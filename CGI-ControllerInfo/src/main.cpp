#include <ControllerState.h>
#include <DeviceAnnouncement.h>
#include <SSTR.h>
#include <SharedHandler.h>
#include <cJSONinclude.h>
#include <cgicc/CgiDefs.h>
#include <cgicc/Cgicc.h>
#include <cgicc/HTTPResponseHeader.h>

int main(int argc, char **argv) {
  try {
    cgicc::Cgicc cgi;

    // Send HTTP header
    cgicc::HTTPResponseHeader response("HTTP/1.1", 200, "application/json");
    response.addHeader("Content-Type", "application/json; charset=utf-8");
    response.addHeader("Pragma-directive", "no-cache");
    response.addHeader("Cache-directive", "no-cache");
    response.addHeader("Cache-control", "no-store");
    response.addHeader("Pragma", "no-cache");
    response.addHeader("expires", "0");
    std::cout << response << std::endl;

    SharedHandler<DeviceAnnouncement> announceReader;
    DeviceAnnouncement slotInfo;
    announceReader.Init(Const::ANNOUNCE_FILE);
    announceReader.Read(&slotInfo);

    ControllerState state;
    SharedHandler<ControllerState> shared;

    int numSlots = Const::MAX_ANNOUNCE_SLOTS;
    int numControllers = slotInfo.numConnected;

    cJSON *root;
    root = cJSON_CreateObject();
    cJSON_AddNumberToObject(root, "numSlots", numSlots);
    cJSON_AddNumberToObject(root, "numControllers", numControllers);

    for (int i(0); i < numControllers; ++i) {
      shared.Init(slotInfo.devices[i].infoFile);
      shared.Read(&state);

      cJSON *controller = cJSON_CreateObject();
      cJSON_AddItemToObject(root, SSTR("slot" << i).c_str(), controller);

      cJSON_AddNumberToObject(controller, "leftTrigger", state.LeftTriggerValue());
      cJSON_AddNumberToObject(controller, "rightTrigger", state.RightTriggerValue());

      cJSON_AddNumberToObject(controller, "leftStickX", state.LeftStickXValue());
      cJSON_AddNumberToObject(controller, "leftStickY", state.LeftStickYValue());

      cJSON_AddNumberToObject(controller, "rightStickX", state.RightStickXValue());
      cJSON_AddNumberToObject(controller, "rightStickY", state.RightStickYValue());

      cJSON_AddBoolToObject(controller, "padLeft", state.PadLeftPressed());
      cJSON_AddBoolToObject(controller, "padUp", state.PadUpPressed());
      cJSON_AddBoolToObject(controller, "padRight", state.PadRightPressed());
      cJSON_AddBoolToObject(controller, "padDown", state.PadDownPressed());

      cJSON_AddBoolToObject(controller, "buttonX", state.XPressed());
      cJSON_AddBoolToObject(controller, "buttonA", state.APressed());
      cJSON_AddBoolToObject(controller, "buttonB", state.BPressed());
      cJSON_AddBoolToObject(controller, "buttonY", state.YPressed());

      cJSON_AddBoolToObject(controller, "leftShoulder", state.LeftShoulderPressed());
      cJSON_AddBoolToObject(controller, "rightShoulder", state.RightShoulderPressed());

      cJSON_AddBoolToObject(controller, "buttonBack", state.BackPressed());
      cJSON_AddBoolToObject(controller, "buttonStart", state.StartPressed());
      cJSON_AddBoolToObject(controller, "buttonXBox", state.LogoPressed());
    }
    std::cout << cJSON_Print(root);
    cJSON_Delete(root);
  } catch (std::exception &e) {
    std::cout << "Error: " << e.what();
  }

  return 0;
}
