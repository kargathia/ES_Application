#! /bin/sh

cat kill_daemon.sh | ssh root@raspberry 2> /dev/null

scp -r www/* root@raspberry:/var/www/
scp CGI-ControllerInfo/bin/CGI-ControllerInfo root@raspberry:/var/www/cgi-bin/controller_info.cgi
scp CGI-ControllerCommands/bin/CGI-ControllerCommands root@raspberry:/var/www/cgi-bin/controller_commands.cgi
scp CGI-ControllerRumble/bin/CGI-ControllerRumble root@raspberry:/var/www/cgi-bin/controller_rumble.cgi

scp Daemon/bin/Daemon root@raspberry:~/ControllerDaemon
#ssh root@raspberry "/root/ControllerDaemon"