#pragma once

#include <SSTR.h>
#include <strcpy_safe.h>
#include <string>

namespace SD {
typedef struct ShareData {
  int val;
  char pronunciation[20];
} ShareData_t;

inline bool Compare(const ShareData_t* left, const ShareData_t* right) {
  bool retval = true;
  if (left->val != right->val) {
    retval = false;
  }
  std::string leftText(left->pronunciation);
  std::string rightText(right->pronunciation);
  if (leftText != rightText) {
    retval = false;
  }

  if (!retval) {
    LOG(WARNING) << left->val << "(" << leftText << ") != " << right->val << "(" << rightText << ")";
  }

  return retval;
}

inline void SetData(ShareData_t* data, int val) {
  data->val = val;
  strcpy_safe(data->pronunciation, SSTR(data->val).c_str());
}
}  // namespace SD
