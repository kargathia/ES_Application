#include <SafeRandom.h>
#include <SharedHandler.h>
#include <glog/logging.h>
#include <pthread.h>
#include <stdlib.h>
#include <strcpy_safe.h>
#include "ShareData.h"
#include "gtest/gtest.h"

typedef struct ThreadArgs {
  pthread_t thread;
  int count;
  bool result;
} ThreadArgs_t;

TEST(SharedHandlerTest, TEST_Sync) {
  std::string file = "shammy";
  SD::ShareData_t input;
  SD::ShareData_t output;
  SharedHandler<SD::ShareData_t> writer;
  SharedHandler<SD::ShareData_t> reader;

  writer.Init(file);
  reader.Init(file);

  SD::SetData(&input, 100);

  writer.Write(&input);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  SD::SetData(&input, 9000);
  writer.Read(&output);
  ASSERT_FALSE(SD::Compare(&input, &output));

  writer.Write(&input);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
  writer.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
}

void* AsyncRunner(void* threadArgs) {
  ThreadArgs_t* args = (ThreadArgs_t*)threadArgs;
  std::string file = "shammy";
  SharedHandler<SD::ShareData_t> shared;
  shared.Init(file);

  SD::ShareData_t input;
  SD::ShareData_t output;

  args->result = true;

  for (int i(0); i < args->count; ++i) {
    input.val = SafeRandom::Int();
    strcpy_safe(input.pronunciation, SSTR(input.val).c_str());
    shared.Write(&input);
    int sleepTime = SafeRandom::Int(1, 100);
    usleep(sleepTime);
    shared.Read(&output);
    bool result = (SSTR(output.val) == SSTR(output.pronunciation));
    args->result = args->result && result;
    if (!result) {
      LOG(WARNING) << "incorrect ShareData: " << output.val << " != " << output.pronunciation;
    }
  }

  return args;
}

TEST(SharedHandlerTest, TEST_Async) {
  std::vector<ThreadArgs_t> threads(3);

  for (auto& t : threads) {
    t.count = 1000;
    t.result = false;
    int status = pthread_create(&t.thread, NULL, &AsyncRunner, &t);
    if (status != 0) {
      LOG(ERROR) << "pthread_create() failed" << strerror(status);
      FAIL();
    }
  }

  for (auto& t : threads) {
    pthread_join(t.thread, NULL);
    ASSERT_TRUE(t.result);
  }
}

TEST(SharedHandlerTest, TEST_Persistence) {
  std::string file = "shammy";
  SD::ShareData_t input;
  SD::ShareData_t output;
  SharedHandler<SD::ShareData_t> original;
  SharedHandler<SD::ShareData_t> floatey;

  input.val = SafeRandom::Int();
  strcpy_safe(input.pronunciation, SSTR(input.val).c_str());

  original.Init(file);
  original.Write(&input);

  floatey.Init(file);
  floatey.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  floatey.Shutdown();
  ASSERT_THROW(floatey.Write(&input), std::invalid_argument);
  ASSERT_THROW(floatey.Read(&output), std::invalid_argument);
  ASSERT_TRUE(SD::Compare(&input, &output));

  original.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  floatey.Init(file);
  floatey.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  original.Shutdown();
  floatey.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
  floatey.Shutdown();

  original.Init(file);
  original.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
  floatey.Init(file);
  floatey.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  floatey.Destroy();
  floatey.Init(file);
  floatey.Read(&output);
  ASSERT_FALSE(SD::Compare(&input, &output));

  SD::SetData(&input, input.val + 1);

  floatey.Write(&input);
  original.Read(&output);
  ASSERT_FALSE(SD::Compare(&input, &output));
}
