#include <QueueHandler.h>
#include <SafeRandom.h>
#include <glog/logging.h>
#include <pthread.h>
#include <stdlib.h>
#include <strcpy_safe.h>
#include <cstdio>
#include "ShareData.h"
#include "gtest/gtest.h"

class QueueHandlerTest : public ::testing::Test {
 public:
  QueueHandlerTest() {}

  void SetUp() {
    file = SSTR("QueueHandlerTest_queue");
    system(SSTR("rm " << file << " 2> /dev/null").c_str());
    writer.Init(file);
    reader.Init(file);
    SD::SetData(&input, SafeRandom::Int());
    SD::SetData(&second, SafeRandom::Int());
    SD::SetData(&output, SafeRandom::Int());
  }

  void TearDown() { system(SSTR("rm " << file << " 2> /dev/null").c_str()); }

  ~QueueHandlerTest() {}

 protected:
  std::string file;
  QueueHandler<SD::ShareData> writer;
  QueueHandler<SD::ShareData> reader;
  SD::ShareData input;
  SD::ShareData second;
  SD::ShareData output;
};

TEST_F(QueueHandlerTest, TEST_ReadWrite) {
  writer.Write(&input);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  writer.Write(&input);
  writer.Write(&second);

  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&second, &output));

  writer.Write(&input);
  writer.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
}

TEST_F(QueueHandlerTest, TEST_Multi) {
  QueueHandler<SD::ShareData> copier;
  copier.Init(file);

  // Check multiple readers
  writer.Write(&input);
  writer.Write(&second);

  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
  copier.Read(&output);
  ASSERT_TRUE(SD::Compare(&second, &output));

  // Check multiple writers
  reader.Write(&input);
  copier.Write(&second);

  writer.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
  writer.Read(&output);
  ASSERT_TRUE(SD::Compare(&second, &output));
}

TEST_F(QueueHandlerTest, TEST_Persistence) {
  writer.Write(&input);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  // Reiniting multiple times is safe
  writer.Init(file);
  writer.Init(file);
  SD::SetData(&input, input.val + 1);
  writer.Write(&input);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  // Shutting down pipe on one end leaves the queue itself intact
  SD::SetData(&input, input.val + 1);
  writer.Write(&input);
  writer.Write(&input);
  writer.Shutdown(false);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  // Writer also can access queue after re-init
  writer.Init(file);
  writer.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  // Destruction will shut down the whole pipe
  SD::SetData(&input, input.val + 1);
  writer.Write(&input);
  writer.Shutdown(true);
  ASSERT_THROW(reader.Read(&output), std::runtime_error);

  // Writer re-init on same file does not re-open same pipe
  writer.Init(file);
  writer.Write(&input);
  ASSERT_THROW(reader.Read(&output), std::runtime_error);

  // Reader will also have to re-init to get the new pipe
  reader.Init(file);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  // Reader can quit and re-init pipe - data will still be there
  SD::SetData(&input, SafeRandom::Int());
  writer.Write(&input);
  reader.Shutdown(false);
  reader.Init(file);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
}

TEST_F(QueueHandlerTest, TEST_SpecificType) {
  writer.Write(&input, 2);
  writer.Write(&second, 3);

  reader.Read(&output, 3);
  ASSERT_TRUE(SD::Compare(&second, &output));

  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
}

TEST_F(QueueHandlerTest, TEST_Priority) {
  writer.Write(&input, 2);
  writer.Write(&second, 3);

  reader.Read(&output, -3);
  ASSERT_TRUE(SD::Compare(&input, &output));
  reader.Read(&output, -3);
  ASSERT_TRUE(SD::Compare(&second, &output));
}

TEST_F(QueueHandlerTest, TEST_FileFaults) {
  std::string badFile = "";
  ASSERT_THROW(reader.Init(badFile), std::invalid_argument);

  reader.Init(file);
  writer.Write(&input);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));

  // Remove backing file
  system(SSTR("rm " << file).c_str());
  // We can still write to it, because the memory is kept in RAM
  writer.Write(&input);
  // We can't init it without creating
  ASSERT_THROW(reader.Init(file, false), std::runtime_error);

  // Recreating the file both allows Init(), and access to the queue
  reader.Init(file);
  reader.Read(&output);
  ASSERT_TRUE(SD::Compare(&input, &output));
}
