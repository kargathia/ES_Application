#pragma once

#include <libusb-1.0/libusb.h>
#include <semaphore.h>
#include <stdexcept>
#include "Context.h"
#include "ControllerConst.h"
#include "ControllerState.h"

class Device {
 public:
  static const int DEFAULT_SEND_TIMEOUT_MS = 0;
  static const int DEFAULT_READ_TIMEOUT_MS = 50;

 public:
  Device();
  virtual ~Device();

  void Init(Context* context, int VID, int PID);
  void Init(libusb_device* dev);
  void Shutdown();

  void Send(Const::ControllerAction action, int timeoutMS = DEFAULT_SEND_TIMEOUT_MS);
  void SendRumble(uint8_t big, uint8_t small, int timeoutMS = DEFAULT_SEND_TIMEOUT_MS);
  bool Read(ControllerState* state, int timeoutMS = DEFAULT_READ_TIMEOUT_MS);

 public:
  libusb_device_handle* Get() {
    return myDevice;
  }

 private:
  void Configure();

 private:
  Device(Device&);

 private:
  libusb_device_handle* myDevice;
  sem_t transferSem;
};
