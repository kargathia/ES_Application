#pragma once

#include <libusb-1.0/libusb.h>
#include "ControllerConst.h"

class Context {
 public:
  Context();
  virtual ~Context();

  libusb_context* Get() {
    return myContext;
  }

 private:
  libusb_context* myContext;
};
