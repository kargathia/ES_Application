#pragma once

#include <algorithm>
#include <string>

namespace Const {

const char* const ANNOUNCE_FILE = "HEAR_YE";
const int MAX_ANNOUNCE_SLOTS = 10;

const int MESSAGE = 0x01;
const int IS = 0x03;

// Type
const int WRITE_ENDPOINT = 0x01;
const int READ_ENDPOINT = 0x81;

const int CONTROLLER_DATA_SIZE = 20;

const int VENDOR_ID = 0x45e;
const int PRODUCT_ID = 0x28e;

enum ControllerAction {
  AllOff = 0x00,
  AllBlinking = 0x01,
  OneFlashesThenOn = 0x02,
  TwoFlashesThenOn = 0x03,
  ThreeFlashesThenOn = 0x04,
  FourFlashesThenOn = 0x05,
  OneOn = 0x06,
  TwoOn = 0x07,
  ThreeOn = 0x08,
  FourOn = 0x09,
  Rotating = 0x0A,
  Blinking = 0x0B,
  SlowBlinking = 0x0C,
  Alternating = 0x0D,

  UNKNOWN = 99999
};

enum SecondByte {
  PadUp = 0x00,
  PadDown = 0x01,
  PadLeft = 0x02,
  PadRight = 0x03,
  StartButton = 0x04,
  BackButton = 0x05,
  LeftStick = 0x06,
  RightStick = 0x07
};

enum ThirdByte { LB = 0x0, RB = 0x1, LogoButton = 0x2, ButtonA = 0x4, ButtonB = 0x5, ButtonX = 0x6, ButtonY = 0x7 };

inline ControllerAction StringToAction(const std::string& actionString) {
  std::string upperString = actionString;
  std::transform(upperString.begin(), upperString.end(), upperString.begin(), ::toupper);

  ControllerAction retval = UNKNOWN;

  if (upperString == "ALLOFF") {
    retval = AllOff;
  } else if (upperString == "ALLBLINKING") {
    retval = AllBlinking;
  } else if (upperString == "ONEFLASHESTHENON") {
    retval = OneFlashesThenOn;
  } else if (upperString == "TWOFLASHESTHENON") {
    retval = TwoFlashesThenOn;
  } else if (upperString == "THREEFLASHESTHENON") {
    retval = ThreeFlashesThenOn;
  } else if (upperString == "ONEON") {
    retval = OneOn;
  } else if (upperString == "TWOON") {
    retval = TwoOn;
  } else if (upperString == "THREEON") {
    retval = ThreeOn;
  } else if (upperString == "FOURON") {
    retval = FourOn;
  } else if (upperString == "ROTATING") {
    retval = Rotating;
  } else if (upperString == "BLINKING") {
    retval = Blinking;
  } else if (upperString == "SLOWBLINKING") {
    retval = SlowBlinking;
  } else if (upperString == "ALTERNATING") {
    retval = Alternating;
  }

  return retval;
}

inline std::string ActionToString(ControllerAction action) {
  std::string retval;
  switch (action) {
    case AllOff: {
      retval = "AllOff";
      break;
    }
    case AllBlinking: {
      retval = "AllBlinking";
      break;
    }
    case OneFlashesThenOn: {
      retval = "OneFlashesThenOn";
      break;
    }
    case TwoFlashesThenOn: {
      retval = "TwoFlashesThenOn";
      break;
    }
    case ThreeFlashesThenOn: {
      retval = "ThreeFlashesThenOn";
      break;
    }
    case FourFlashesThenOn: {
      retval = "FourFlashesThenOn";
      break;
    }
    case OneOn: {
      retval = "OneOn";
      break;
    }
    case TwoOn: {
      retval = "TwoOn";
      break;
    }
    case ThreeOn: {
      retval = "ThreeOn";
      break;
    }
    case FourOn: {
      retval = "FourOn";
      break;
    }
    case Rotating: {
      retval = "Rotating";
      break;
    }
    case Blinking: {
      retval = "Blinking";
      break;
    }
    case SlowBlinking: {
      retval = "SlowBlinking";
      break;
    }
    case Alternating: {
      retval = "Alternating";
      break;
    }
    case UNKNOWN: {
      retval = "UNKNOWN";
      break;
    }
  }
  return retval;
}

};  // namespace Const
