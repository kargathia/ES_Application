#pragma once

#include <stdlib.h>
#include "ControllerConst.h"

struct DeviceInfo {
  char commandFile[40];
  char infoFile[40];
};

struct DeviceAnnouncement {
  size_t numConnected;
  DeviceInfo devices[Const::MAX_ANNOUNCE_SLOTS];
};
