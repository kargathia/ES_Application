#pragma once

#include "ControllerConst.h"

struct ControllerCommand {
  Const::ControllerAction action = Const::UNKNOWN;
  bool rumbleCommand = false;
  uint8_t heavyRumble = 0;
  uint8_t lightRumble = 0;
};
