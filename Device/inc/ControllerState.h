#pragma once

#include <stdint.h>
#include <vector>
#include "ControllerConst.h"

class ControllerState {
 public:
  ControllerState();
  ~ControllerState();

  void SetData(const uint8_t* data);

  bool PadUpPressed() const;
  bool PadDownPressed() const;
  bool PadLeftPressed() const;
  bool PadRightPressed() const;

  bool StartPressed() const;
  bool BackPressed() const;
  bool LogoPressed() const;

  bool LeftStickPressed() const;
  bool RightStickPressed() const;
  bool LeftShoulderPressed() const;
  bool RightShoulderPressed() const;

  bool APressed() const;
  bool BPressed() const;
  bool XPressed() const;
  bool YPressed() const;

  uint8_t LeftTriggerValue() const;
  uint8_t RightTriggerValue() const;

  int16_t LeftStickXValue() const;
  int16_t LeftStickYValue() const;

  int16_t RightStickXValue() const;
  int16_t RightStickYValue() const;

 private:
  int16_t Make16(uint8_t left, uint8_t right) const;
  bool CheckBit(int val, int pos) const;

 private:
  uint8_t backingData[Const::CONTROLLER_DATA_SIZE];
};
