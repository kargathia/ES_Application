# Installation

In order to install the ES_USB Controller driver on the target Raspberry, the following steps should be followed:

* Connect the Raspberry (assumed IP is default: `10.0.0.42`)
* Install BuildRoot on the Raspberry. (Follow the ES instructions to do this)
* Add the BuildRoot compiler directory to the Path
* Clone this repository
* Install Libusb (`sudo apt-get install libusb-1.0-0-dev`)
* CD to repository, and run `make && make install`. Enter Raspberry password when prompted.


# Running

Connect the USB Controller to the Raspberry, and run the following commands:

```
ssh root@10.0.0.42
~/Controller
```

# Youtube

A [YouTube Presentation](https://youtu.be/o-nRI4AfgC4) is also available.

# Code Walkthrough

The main function can be found in `Controller.cpp`. Here a **Context**, **Device**, and **EventHandler** are initialized.

* **Context** acts as a RAII wrapper around the `libusb_context` pointer. 
* **Device** wraps `libusb_device_handle` and reads/sends data to and from the managed device.
* **EventHandler** is provided to **Device**, and implements functions for all types of controller input.

The main will keep on trying to `Read()` in Controller. This is a blocking call that will call the event handler if data is read.

(`Device.cpp:71`)
```
  uint8_t data[20] = {};
  error_code =
      libusb_interrupt_transfer(myDevice, INPUT, data, 20, &transferred, 0);

  if (error_code < 0) {
    throw "Failed to send";
  }
```

As soon as data is received, it is parsed according to the XBox controller spec.

(`Device.cpp:79`)
```
  if (data[0] == 0) {
    ReadByte2((int)data[2], handler);
    ReadByte3((int)data[3], handler);
    ReadByte4((int)data[4], handler);
    ReadByte5((int)data[5], handler);
    ReadByte6(Make16((int)data[6], (int)data[7]), handler);
    ReadByte8(Make16((int)data[8], (int)data[9]), handler);
    ReadByteA(Make16((int)data[10], (int)data[11]), handler);
    ReadByteC(Make16((int)data[12], (int)data[13]), handler);
  }
```

With this info, appropriate functions in the registered EventHandler are called.
For example, if the Logo button is pressed, all lights are set to blinking.

(`EventHandler.cpp:50`)
```
void EventHandler::OnLogoButton(Device* device) {
    std::cout << "XBox Logo" << std::endl;
    device->Send(Const::AllBlinking);
}
```