#include <ControllerState.h>
#include <string.h>
#include "ControllerConst.h"

ControllerState::ControllerState() {
  memset(&backingData[0], 0, Const::CONTROLLER_DATA_SIZE);
}

ControllerState::~ControllerState() {
}

void ControllerState::SetData(const uint8_t* data) {
  memcpy(&backingData[0], data, Const::CONTROLLER_DATA_SIZE);
}

int16_t ControllerState::Make16(uint8_t left, uint8_t right) const {
  int16_t output = left;
  output += right << 8;
  return output;
}

bool ControllerState::CheckBit(int val, int pos) const {
  return (val >> pos) & 1;
}

bool ControllerState::PadUpPressed() const {
  return CheckBit(backingData[2], Const::PadUp);
}

bool ControllerState::PadDownPressed() const {
  return CheckBit(backingData[2], Const::PadDown);
}

bool ControllerState::PadLeftPressed() const {
  return CheckBit(backingData[2], Const::PadLeft);
}

bool ControllerState::PadRightPressed() const {
  return CheckBit(backingData[2], Const::PadRight);
}

bool ControllerState::StartPressed() const {
  return CheckBit(backingData[2], Const::StartButton);
}

bool ControllerState::BackPressed() const {
  return CheckBit(backingData[2], Const::BackButton);
}

bool ControllerState::LogoPressed() const {
  return CheckBit(backingData[3], Const::LogoButton);
}

bool ControllerState::LeftStickPressed() const {
  return CheckBit(backingData[2], Const::LeftStick);
}

bool ControllerState::RightStickPressed() const {
  return CheckBit(backingData[2], Const::RightStick);
}

bool ControllerState::LeftShoulderPressed() const {
  return CheckBit(backingData[3], Const::LB);
}

bool ControllerState::RightShoulderPressed() const {
  return CheckBit(backingData[3], Const::RB);
}

bool ControllerState::APressed() const {
  return CheckBit(backingData[3], Const::ButtonA);
}

bool ControllerState::BPressed() const {
  return CheckBit(backingData[3], Const::ButtonB);
}

bool ControllerState::XPressed() const {
  return CheckBit(backingData[3], Const::ButtonX);
}

bool ControllerState::YPressed() const {
  return CheckBit(backingData[3], Const::ButtonY);
}

uint8_t ControllerState::LeftTriggerValue() const {
  return backingData[4];
}

uint8_t ControllerState::RightTriggerValue() const {
  return backingData[5];
}

int16_t ControllerState::LeftStickXValue() const {
  return Make16(backingData[6], backingData[7]);
}

int16_t ControllerState::LeftStickYValue() const {
  return Make16(backingData[8], backingData[9]);
}

int16_t ControllerState::RightStickXValue() const {
  return Make16(backingData[10], backingData[11]);
}

int16_t ControllerState::RightStickYValue() const {
  return Make16(backingData[12], backingData[13]);
}
