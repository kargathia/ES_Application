#include <libusb-1.0/libusb.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <string>

#include "Context.h"
#include "ControllerConst.h"
#include "ControllerState.h"
#include "Device.h"

// namespace {
// int VID = 0x45e;
// int PID = 0x28e;
// }

// int main() {
//   daemon(0, 0);

//   Context context = Context();
//   Device device(context, VID, PID);

//   ControllerState handler = ControllerState();
//   while (true) {
//     device.Read(handler);
//     // handler.write()
//   }

//   return 0;
// }
