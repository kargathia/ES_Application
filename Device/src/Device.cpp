#include <QueueHandler.h>
#include <glog/logging.h>
#include <iostream>
#include <stdexcept>
#include "Context.h"
#include "Device.h"

Device::Device() : myDevice(NULL) {
}

Device::~Device() {
  Shutdown();
}

void Device::Init(Context* context, int VID, int PID) {
  Shutdown();
  // open device with known values for VID and PID
  myDevice = libusb_open_device_with_vid_pid(context->Get(), VID, PID);
  Configure();
}

void Device::Init(libusb_device* dev) {
  Shutdown();
  libusb_open(dev, &myDevice);
  Configure();
}

void Device::Configure() {
  // find out if kernel driver is attached
  if (libusb_kernel_driver_active(myDevice, 0) == 1) {
    if (libusb_detach_kernel_driver(myDevice, 0) == 0) {
      LOG(INFO) << "Driver detached";
    }
  }

  if (libusb_claim_interface(myDevice, 0) < 0) {
    throw std::runtime_error("Failed to claim device");
  }

  sem_init(&transferSem, 0, 1);
}

void Device::Shutdown() {
  if (myDevice != NULL) {
    sem_wait(&transferSem);
    LOG(INFO) << "closing device " << myDevice;
    libusb_release_interface(myDevice, 0);
    if (libusb_kernel_driver_active(myDevice, 0)) {
      libusb_attach_kernel_driver(myDevice, 0);
    }
    libusb_close(myDevice);
    myDevice = NULL;
    sem_close(&transferSem);
  }
}

void Device::Send(Const::ControllerAction action, int timeoutMS) {
  int error_code = 0;
  int transferred = 0;

  unsigned char data[] = {Const::MESSAGE, Const::IS, (uint8_t)action};
  sem_wait(&transferSem);
  error_code = libusb_interrupt_transfer(myDevice, Const::WRITE_ENDPOINT, data, sizeof(data), &transferred, timeoutMS);
  sem_post(&transferSem);

  if (error_code < 0) {
    throw std::runtime_error("Failed to send");
  }
}

void Device::SendRumble(uint8_t big, uint8_t small, int timeoutMS) {
  int error_code = 0;
  int transferred = 0;

  uint8_t data[] = {0x00, 0x08, 0x00, big, small, 0x00, 0x00, 0x00};

  sem_wait(&transferSem);
  error_code = libusb_interrupt_transfer(myDevice, Const::WRITE_ENDPOINT, data, sizeof(data), &transferred, timeoutMS);
  sem_post(&transferSem);

  if (error_code < 0) {
    throw std::runtime_error("Failed to send");
  }
}

bool Device::Read(ControllerState* state, int timeoutMS) {
  int error_code = 0;
  int transferred = 0;
  bool success = true;

  uint8_t data[Const::CONTROLLER_DATA_SIZE];
  memset(&data[0], 0, sizeof(data));

  sem_wait(&transferSem);
  error_code = libusb_interrupt_transfer(myDevice, Const::READ_ENDPOINT, data, Const::CONTROLLER_DATA_SIZE,
                                         &transferred, timeoutMS);
  sem_post(&transferSem);

  if (error_code < 0 && error_code != LIBUSB_ERROR_TIMEOUT) {
    throw std::runtime_error("Failed to read");
  }

  if (transferred == Const::CONTROLLER_DATA_SIZE) {
    state->SetData(&data[0]);
    success = true;
  }

  return success;
}
